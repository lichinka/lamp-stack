# lamp-stack

PHP + MySQL using Docker Compose.-

## Installing additional PHP modules, e.g., for PDO_MySQL, MySQL and MySQLi:
```
$ docker-compose up -d
$ docker exec -it php_web /bin/bash
$ docker-php-ext-install -j$(nproc) pdo_mysql mysql mysqli
$ docker-compose stop
```

## Usage
1. Link your local PHP code as a volume inside the YAML file, e.g.:
```
(...)
    volumes:
        - /home/[user]/tmp/:/var/www/html/
(...)

```
2. Bring the containers up:
```
$> docker-compose up
```

3. Stopping the services:
```
$> docker-compose stop
```
